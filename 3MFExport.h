// 3MFExport.h: interface for the C3DMExport class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_3MFEXPORT_H__C9498A28_8E7C_4DEC_9E28_75C6FC180D7F__INCLUDED_)
#define AFX_3MFEXPORT_H__C9498A28_8E7C_4DEC_9E28_75C6FC180D7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"

#ifndef GENERATE_DEFINED
#include "generate.h"
#endif

//JM: implements 3dm2TC export 

typedef std::pair <CString, int> layerInfo;
typedef std::pair <CString, int> lineTypeInfo;
typedef std::pair <CString, int> materialInfo;


////////////////////////////////////////////
//additional class, used toi collect current group info

class C3MFExport : public CGenerator
{
public:
	C3MFExport(LPCWSTR file, LPVOID lpStorage, hApp App, hGraphic Graphic, hDrawing Drawing, GraphicFlags Flags);
	virtual ~C3MFExport();

	int _3MFExport();
private:
	typedef enum { CIRCLE, ARC, ELLIPSE, ELLIPTIC_ARC } CURVE_TYPE;

	enum TCFaceType
	{
		SPLINE = 0,
		PLANAR = 1,
		CONE = 2,
		SPHERE = 3,
		TORUS = 4
	};
};

#endif // !defined(AFX_3DMEXPORT_H__5B5517A7_1FD3_441F_975C_74CDA6644F9A__INCLUDED_)

