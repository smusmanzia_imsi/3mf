#include "StdAfx.h"
//#include "SettingStructs.h"
//#include "3dm.rh"
#include "tcxmlini.h"

//#ifdef TC17_LOC
#include "GetLangExt.cpp"
//#endif

#include "error.h"

extern HINSTANCE	hInstRes;

//static ExportSettings gExportSettings;

/////////////////////////////////////////////////////////////////////////
extern "C" BOOL
CALLBACK ExpDlgP(HWND hDlg, UINT message, UINT wParam, LONG lParam)
{
	//switch (message)
	//{
	//case WM_INITDIALOG:
	//	//read settings
	//	LoadSettings(&gExportSettings, NULL);

	//	//set
	//	CheckDlgButton(hDlg, IDC_COPYTEXTURES, gExportSettings.m_bCopyTextures ? BST_CHECKED : BST_UNCHECKED);
	//	//	CheckDlgButton( hDlg, IDC_EXPORTPOINTS, gExportSettings.m_bExportPointGeometry?BST_CHECKED:BST_UNCHECKED );
	//	CheckDlgButton(hDlg, IDC_EXPORTTEXT, gExportSettings.m_bExportText ? BST_CHECKED : BST_UNCHECKED);
	//	CheckDlgButton(hDlg, IDC_EXPORTASSURFACE, gExportSettings.m_bExportAsSurface ? BST_CHECKED : BST_UNCHECKED);

	//	if (IFDEF(TIO))
	//	{
	//		HWND hOkButton = GetDlgItem(hDlg, IDOK);
	//		HWND hCancelButton = GetDlgItem(hDlg, IDCANCEL);
	//		HWND hHelpButton = GetDlgItem(hDlg, 9);
	//		CRect rect;
	//		CPoint point;

	//		ShowWindow(hHelpButton, FALSE);
	//		GetWindowRect(hCancelButton, &rect);
	//		point = rect.TopLeft();
	//		ScreenToClient(hDlg, &point);
	//		SetWindowPos(hOkButton, NULL, point.x, point.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW);
	//		GetWindowRect(hHelpButton, &rect);
	//		point = rect.TopLeft();
	//		ScreenToClient(hDlg, &point);
	//		SetWindowPos(hCancelButton, NULL, point.x, point.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW);

	//		HWND hWnd = NULL;
	//		hWnd = GetDlgItem(hDlg, IDC_COPYTEXTURES);
	//		ShowWindow(hWnd, FALSE);

	//		LONG lExStyle = GetWindowLong(hDlg, GWL_EXSTYLE);
	//		lExStyle &= (~WS_EX_CONTEXTHELP);
	//		SetWindowLong(hDlg, GWL_EXSTYLE, lExStyle);
	//	}

	//	return TRUE;

	//case WM_COMMAND:
	//	switch (LOWORD(wParam))
	//	{
	//	case IDCANCEL:
	//		EndDialog(hDlg, 0);
	//		break;

	//	case IDOK:
	//	{
	//		if (IsDlgButtonChecked(hDlg, IDC_EXPORTTEXT) == BST_CHECKED)
	//			gExportSettings.m_bExportText = true;
	//		else
	//			gExportSettings.m_bExportText = false; //explode text

	//		if (IsDlgButtonChecked(hDlg, IDC_COPYTEXTURES) == BST_CHECKED)
	//			gExportSettings.m_bCopyTextures = true;
	//		else
	//			gExportSettings.m_bCopyTextures = false;

	//		if (IsDlgButtonChecked(hDlg, IDC_EXPORTASSURFACE) == BST_CHECKED)
	//			gExportSettings.m_bExportAsSurface = true;
	//		else
	//			gExportSettings.m_bExportAsSurface = false; //solid as surface

	//														//	if( IsDlgButtonChecked( hDlg, IDC_EXPORTPOINTS ) == BST_CHECKED )												
	//														//		gExportSettings.m_bExportPointGeometry = true; //all points exported as points
	//														//	else
	//														//		gExportSettings.m_bExportPointGeometry = false; //export point geometry

	//														//save settings
	//		SaveSettings(&gExportSettings, NULL);

	//		EndDialog(hDlg, 0);
	//	}
	//	break;

	//	case IDHELP:
	//		break;
	//	}
	//	return TRUE;
	//}
	return FALSE;
}// ExpP //

 ////////////////////////////////////////////////////////////////////

extern "C"
BOOL FILTERAPI _3MFSETUP(hApp App, BOOL RW)
{
	//BOOL Result = TRUE;

	//if (AppGetPrivateProfile(AppGetCurrentApp()) == NULL)
	//	return FALSE;

	//if (RW)
	//{
	//	TCHAR szCaption[64] = { _T("3DM ") };
	//	TCHAR szText[1024];


	//	::LoadString(hInstRes, IDS_DEFAULT_IMPORT_SETUP_TITLE, szCaption + 4, 64);
	//	//#ifdef TC17_LOC
	//	if (IFDEF(TC17_LOC))
	//	{
	//		if (Is_French())
	//		{
	//			::LoadString(hInstRes, IDS_DEFAULT_IMPORT_SETUP_TITLE, szCaption, sizeof(szCaption));
	//			_tcscat(szCaption, _T(" 3DM"));
	//		}
	//		//#endif
	//	}
	//	::LoadString(hInstRes, IDS_DEFAULT_IMPORT_SETUP_PROMPT, szText, 1024);

	//	::MessageBox(GetActiveWindow(), szText, szCaption, 0);

	//	return Result;
	//}
	//else
	//{
	//	DLGPROC dlg = (DLGPROC)MakeProcInstance((FARPROC)&ExpDlgP, hInst);

	//	HWND hwnd = GetActiveWindow();

	//	HRSRC hRes = ::FindResource(hInstRes, "#102", RT_DIALOG);//# Non-localizable string# 
	//	if (hRes == NULL)
	//		return -1; //DialogBox fails
	//	HGLOBAL hTemplateMem = ::LoadResource(hInstRes, hRes);
	//	if (hTemplateMem == NULL)
	//		return -1; //DialogBox fails
	//	LPDLGTEMPLATE hTemplate = (LPDLGTEMPLATE)::LockResource(hTemplateMem);
	//	Result = DialogBoxIndirect(hInst, hTemplate, hwnd, dlg) == IDOK;

	//	FreeProcInstance((FARPROC)dlg);
	//}
	//return Result;
	return TRUE;
}// 3DMSETUP //


 ////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////

//BOOL LoadSettings(ExportSettings* es, LPCWSTR szINIFileName)
//{
	//int val;// = dbGetPrivateProfileInt(
	//		// "3DM_Export", "ExportPointGeom", -1, szINIFileName);

	//		//if( val == -1 )
	//		//	return FALSE;

	//		//es->m_bExportPointGeometry = (bool)val;

	//val = dbGetPrivateProfileInt(
	//	"3DM_Export", "CopyTextures", -1, szINIFileName);

	//if (val == -1)
	//	return FALSE;

	//es->m_bCopyTextures = val != 0;

	//val = dbGetPrivateProfileInt(
	//	"3DM_Export", "ExportText", -1, szINIFileName);

	//if (val == -1)
	//	return FALSE;

	//es->m_bExportText = val != 0;

	//val = dbGetPrivateProfileInt(
	//	"3DM_Export", "ExportAsSurface", -1, szINIFileName);

	//if (val == -1)
	//	return FALSE;

	//es->m_bExportAsSurface = val != 0;

	//return TRUE;
//}

//BOOL SaveSettings(ExportSettings* es, LPCWSTR szINIFileName)
//{
	//int rc;
	////	rc = dbWritePrivateProfileString("3DM_Export", "ExportPointGeom",//# Non-localizable string#
	////		es->m_bExportPointGeometry ? "1" : "0", szINIFileName);
	////	if (rc == 0)
	////		return FALSE;	// File write error

	//rc = dbWritePrivateProfileString("3DM_Export", "CopyTextures",//# Non-localizable string#
	//	es->m_bCopyTextures ? "1" : "0", szINIFileName);
	//if (rc == 0)
	//	return FALSE;

	//rc = dbWritePrivateProfileString("3DM_Export", "ExportText",//# Non-localizable string#
	//	es->m_bExportText ? "1" : "0", szINIFileName);
	//if (rc == 0)
	//	return FALSE;

	//rc = dbWritePrivateProfileString("3DM_Export", "ExportAsSurface",//# Non-localizable string#
	//	es->m_bExportAsSurface ? "1" : "0", szINIFileName);
	//if (rc == 0)
	//	return FALSE;

	//return TRUE;
//}

////////////////////////////////////////////////////////
//BOOL ExportDlgDataSetup(hApp App, ExportSettings* es)
//{
	//if (AppGetPrivateProfile(AppGetCurrentApp()) != NULL)
	//{	// Setup from .INI file
	//	if (LoadSettings(es, NULL))
	//		return TRUE;
	//}

	////set default values
	//es->m_bCopyTextures = false;
	////	es->m_bExportPointGeometry = true;
	//es->m_bExportText = false;
	//es->m_bExportAsSurface = true;

	//return FALSE;
//}

