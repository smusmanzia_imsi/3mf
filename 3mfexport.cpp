#include <tchar.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <atlbase.h>
#include <algorithm>
#include "StdAfx.h"
#include "3MFExport.h"
#include "Model\COM\NMR_COMInterfaces.h"
#include "Model\COM\NMR_COMVersion.h"
//#include "NMR_COMFactory.h"


C3MFExport::C3MFExport(LPCWSTR file, LPVOID lpStorage, hApp App, hGraphic Graphic, hDrawing Drawing, GraphicFlags Flags) :
	CGenerator(App,Graphic, Drawing, NULL,Flags, file)
{
	int a = _3MFExport();
}

C3MFExport::~C3MFExport()
{

}

using namespace NMR;
extern "C"
{
	LIB3MF_DECLSPEC HRESULT NMRCreateModelFactory(_Outptr_ ILib3MFModelFactory ** ppFactory) 
	{
		return LIB3MF_DECLSPEC HRESULT();
	}
}

int C3MFExport::_3MFExport()
{
	HRESULT hResult;
	DWORD nInterfaceVersion;

	// Objects
	CComPtr<ILib3MFModel> pModel;
	CComPtr<ILib3MFModelFactory> pFactory;
	CComPtr<ILib3MFModelWriter> pSTLWriter;
	CComPtr<ILib3MFModelWriter> p3MFWriter;
	CComPtr<ILib3MFModelMeshObject> pMeshObject;
	CComPtr<ILib3MFModelBuildItem> pBuildItem;


	std::cout << "------------------------------------------------------------------" << std::endl;
	std::cout << "3MF Cube example" << std::endl;
	std::cout << "------------------------------------------------------------------" << std::endl;

	// Initialize COM
	hResult = CoInitialize(nullptr);
	if (hResult != S_OK) {
		std::cout << "could not initialize COM: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Create Factory Object
	hResult = NMRCreateModelFactory(&pFactory);
	if (hResult != S_OK) {
		std::cout << "could not get Model Factory: " << std::hex << hResult << std::endl;
		return -1;
	}


	// Check 3MF Library Version
	hResult = pFactory->GetInterfaceVersion(&nInterfaceVersion);
	if (hResult != S_OK) {
		std::cout << "could not get 3MF Library version: " << std::hex << hResult << std::endl;
		return -1;
	}

	if ((nInterfaceVersion != NMR_APIVERSION_INTERFACE)) {
		std::cout << "invalid 3MF Library version: " << nInterfaceVersion << std::endl;
		std::cout << "3MF Library version should be: " << NMR_APIVERSION_INTERFACE << std::endl;
		return -1;
	}

	// Create Model Instance
	hResult = pFactory->CreateModel(&pModel);
	if (hResult != S_OK) {
		std::cout << "could not create model: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Create Mesh Object
	hResult = pModel->AddMeshObject(&pMeshObject);
	if (hResult != S_OK) {
		std::cout << "could not add mesh object: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Set custom name
	hResult = pMeshObject->SetName(L"Cube");
	if (hResult != S_OK) {
		std::cout << "could not set object name: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Create mesh structure of a cube
	MODELMESHVERTEX pVertices[8];
	MODELMESHTRIANGLE pTriangles[12];
	float fSizeX = 100.0f;
	float fSizeY = 200.0f;
	float fSizeZ = 300.0f;


	hResult = pMeshObject->SetGeometry(pVertices, 8, pTriangles, 12);
	if (hResult != S_OK) {
		std::cout << "could not set mesh geometry: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Add Build Item for Mesh
	hResult = pModel->AddBuildItem(pMeshObject, nullptr, &pBuildItem);
	if (hResult != S_OK) {
		std::cout << "could not create build item: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Output cube as STL and 3MF

	// Create Model Writer
	hResult = pModel->QueryWriter("stl", &pSTLWriter);
	if (hResult != S_OK) {
		std::cout << "could not create model reader: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Export Model into File
	std::cout << "writing cube.stl..." << std::endl;
	hResult = pSTLWriter->WriteToFile(L"cube.stl");
	if (hResult != S_OK) {
		std::cout << "could not write file: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Create Model Writer
	hResult = pModel->QueryWriter("3mf", &p3MFWriter);
	if (hResult != S_OK) {
		std::cout << "could not create model reader: " << std::hex << hResult << std::endl;
		return -1;
	}

	// Export Model into File
	std::cout << "writing cube.3mf..." << std::endl;
	hResult = p3MFWriter->WriteToFile(L"cube.3mf");
	if (hResult != S_OK) {
		std::cout << "could not write file: " << std::hex << hResult << std::endl;
		return -1;
	}

	std::cout << "done" << std::endl;
	std::cin.get();

	return 0;
}