// 3mfmain.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "resource.h"
#include <Windows.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include <atlbase.h>
#include <algorithm>
#include "3MFExport.h"

// COM Includes of 3MF Library
//#include "Model\COM\NMR_COMFactory.h"
#include "NMR_COMFactory.h"
int _3MFExport();

//LIB3MF_DECLSPEC HRESULT NMR::NMRCreateModelFactory(_Outptr_ ILib3MFModelFactory ** ppFactory)
//{
//	return LIB3MF_DECLSPEC HRESULT();
//}
HINSTANCE	hInst;
HINSTANCE	hInstRes;

BOOL APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpvReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		hInst = hInstance;
		hInstRes = LoadResExtDLL(hInst, FALSE);
		if (hInstRes == NULL)
			hInstRes = hInst;
		break;

	case DLL_THREAD_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		if (hInstRes != hInst)
		{
			::FreeLibrary(hInstRes);
			hInstRes = hInst;
		}
		break;
	case DLL_THREAD_DETACH:
		break;
	} // end of switch

	return 1;   // ok
} // DllMain  //


extern "C" _EXPORTENTRY
int FILTERAPI FilterInit(SAFEARRAY FAR* FAR* ppsaFilterInfo)
{
	//MessageBox(NULL,_T("3dm filer init 1"), _T("3dm"),MB_OK);
	ASSERT(ppsaFilterInfo != NULL);
	ASSERT(*ppsaFilterInfo == NULL);


#define NUM_FILTERS 1
	LONG where = 0;

	// Make an array of the filter info strings
	TCHAR szDesc[256];
	szDesc[0] = '\0';
	*ppsaFilterInfo = SafeArrayCreateVector(VT_BSTR, 0, NUM_FILTERS);

	//	_tcscpy(szDesc, _T("IMSIGX.3DM.4,TurboCAD for .3dm file,0,100,3DM,3DM"));
	LoadString(hInstRes, IDS_3MF_DESCRIPTION, szDesc, sizeof(szDesc));

	BSTR bstr = SysAllocStringByteLen(szDesc, lstrlen(szDesc));
	SafeArrayPutElement(*ppsaFilterInfo, &where, bstr);
	SysFreeString(bstr);
	//MessageBox(NULL,_T("3dm filer init"), _T("3dm"),MB_OK);

	return NUM_FILTERS;
}

extern "C"
long FILTERAPI _3MFCHECKW(LPCWSTR lpszFile, LPVOID lpArchive)
{
	//ON::Begin();
	//ON_Workspace ws;
	//FILE* fp = ws.OpenFile(lpszFile, L"rb");
	//if (fp == NULL)
	//	return ER_OPENFILE;
	//ON_BinaryFile archive(ON::read3dm, fp);

	//if ((archive.Archive3dmVersion() > 0) || (archive.Archive3dmVersion() < 3))
	//{
	//	int version = 0;
	//	ON_String comment_block;
	//	if (archive.Read3dmStartSection(&version, comment_block))
	//		return 0;
	//}

	return ER_READERROR;
}

extern "C"
int FILTERAPI _3MFEXPLAIN(long code, LPSTR lpError, int nBufLen)
{
	UINT	ID;
	int	ret;

	switch (code)
	{
		//	case ER_READWMF:
		//		ID = IDS_READWMF;
		//		break;

		//	case ER_UNEXPECTEOF:
		//		ID = IDS_UNEXPECTEOF;
		//		break;

	case ER_READERROR:
		ID = IDS_READERROR;
		break;

		//	case ER_NOTENOUGHMEMORY:
		//		ID = IDS_NOTENOUGHMEMORY;
		//		break;

	case ER_OPENFILE:
		ID = IDS_OPENFILEERROR;
		break;

	case ER_WRITERROR:
		ID = IDS_WRITERROR;
		break;

		//	case ER_OLE:
		//		ID = IDS_OLEERROR;
		//		break;

	default:
		ID = IDS_ERRORUNKNOWN;
	}

	ret = LoadString(hInstRes, ID, lpError, nBufLen);
	return ret;
}

//-----------------------------------------------------------------------------------------------//
// 3MFREAD                                                                             			 //
//-----------------------------------------------------------------------------------------------//
extern "C"
long FILTERAPI _3MFREADW(LPCWSTR lpszFile, LPVOID lpszStream, hGraphic gRoot, hDrawing Drawing, hApp App, hMatrix m, BOOL bSelect)
{
	//C3DMImport import(lpszFile, App, gRoot, Drawing);
	//return import.Run();
	return 1;
}

extern "C"
long FILTERAPI _3MFWRITEW(LPCWSTR lpszFile, LPVOID lpStorage, hGraphic Graphic,
	hDrawing Drawing, hApp App, GraphicFlags Flags)
{
	try
	{
		C3MFExport exportfile(lpszFile,lpStorage,App,Graphic,Drawing,Flags);
	}

	catch (CString err)
	{
#ifdef _DEBUG
			MessageBox(NULL, err, _T("3mf export error"), MB_OK);
#endif
	}
	return ER_WRITERROR;
}
