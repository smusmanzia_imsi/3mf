// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3E175280_F295_40EA_899F_FBF9234248FC__INCLUDED_)
#define AFX_STDAFX_H__3E175280_F295_40EA_899F_FBF9234248FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if _MSC_VER >= 1400 
#include "vcdeprecate.h"
#endif
// Insert your headers here
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

//#include <windows.h>
#if !defined(_AFXDLL)
#define _AFXDLL
#endif
#ifdef _AFXDLL
#include <afxwin.h> 
#include <afxole.h>
#else
#include <windows.h>
#include <string.h>
#include <math.h>    
#include <float.h>
#include <commdlg.h>
#include <stdio.h> 
#endif   


#include "dbapi.h"
#include "property.h"
#include "propini.h"
#include "apitypes.h"
#include "tc25ini.h"
#include "tcw_def.h"
#include "dbapiadd.h"
#include "importcw.h"

#ifndef UNICODE
#undef LOGFONT
#define LOGFONT LOGFONTW
#endif

//#include "opennurbs.h"

#ifndef UNICODE
#undef LOGFONT
#define LOGFONT LOGFONTA
#endif


#include  <math.h>
#include <vector>
#include <algorithm>
#include <memory>
//#include <iostream>

#pragma warning(push,3)
#pragma warning(disable: 4097)
#include <strstream>	// AOS 081403
#pragma warning(pop)

#define PI	3.141592653589793238

#define ER_READERROR		-3L
#define ER_WRITERROR		-4L
#define ER_OPENFILE		-6L
#define ER_ERRORUNKNOWN		-7L

#include <map>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3E175280_F295_40EA_899F_FBF9234248FC__INCLUDED_)
