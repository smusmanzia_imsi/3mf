//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 3mf.rc
//
#define IDS_3MF_DESCRIPTION             100
#define IDS_READERROR                   2
#define IDS_WRITERROR                   3
#define IDS_OPENFILEERROR               6
#define IDS_ERRORUNKNOWN                7

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
